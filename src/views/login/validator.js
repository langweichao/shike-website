import { EXG_CHINESE, EXG_EMALIL, EXG_PHONE, EXG_PASSWORD_SPECIAL } from '@/utils/constant'

// 账号校验 只能汉字
export const CHECK_USERNAME_CH = (rule, value, callback) => {
  // 汉字校验
  if (!value) callback(new Error('请输入用户姓名'))
  else if (!EXG_CHINESE.test(value)) callback(new Error('请输入汉字'))
  else callback()
}

// 邮箱校验
export const CHECK_EMAIL = (rule, value, callback) => {
  if (value && !EXG_EMALIL.test(value)) callback(new Error('请输入正确的邮箱'))
  else callback()
}

// 账号校验 只能有数字 字母组成
export const CHECK_ACCOUNT = (rule, value, callback) => {
  // 英文或数字或中划线
  let reg = /^[A-Za-z0-9-]+$/
  if (!value) callback(new Error('请输入登录用户名'))
  else if (!reg.test(value)) callback(new Error('只能由数字或字母组合'))
  else if (value.slice(-6) !== '-admin') callback(new Error("请以'-admin'结尾"))
  else callback()
}

// 手机号校验
export const CHECK_PHONE = (rule, value, callback) => {
  if (!value) callback(new Error('请输入手机号'))
  else if (!EXG_PHONE.test(value)) callback(new Error('请输入正确的手机号'))
  else callback()
}

// 校验密码
export const CHECK_PASSWORD = (rule, value, callback) => {
  if (!value) callback(new Error('请输入密码'))
  else if (!EXG_PASSWORD_SPECIAL.test(value)) callback(new Error('输入8位及以上数字、字母和特殊符号组合密码'))
  else callback()
}

// 校验密码
// 里面的this需要重新绑定，固不能使用箭头函数
export function CHECK_REPEAT (rule, value, callback) {
  if (!value) callback(new Error('请再次输入密码'))
  else if (value !== this.form.password) callback(new Error('两次密码不一致'))
  else if (!EXG_PASSWORD_SPECIAL.test(value)) callback(new Error('输入8位及以上数字、字母和特殊符号组合密码'))
  else callback()
}
