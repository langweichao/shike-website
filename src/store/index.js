import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    headerClass:true,


  },
  getters: {
  },
  mutations: {
    // 设置数据
    setState ({ key, value }) {
      store[key] = value
    },
  },
  actions: {
  },
  modules: {
  }
})
