import { Authority } from '../request'

const { post, get } = Authority
// 首页广告
export const homeList = params => post('/business/dutuiHome', params )
export const homeMenuList = params => get('/business/homeMenu', {params} )

// 产品动态
export const dynamicsList = params => post('/business/productDynamics',  params )
// 数据趋势
export const datastatisticsList = params => post('/business/dataStatistics',  params )
// 账户信息
export const accountData = params => post('/business/accountInfo', params )
// （账户概况（）
export const getaccount= params => post('/business/account', params )

//
// 财务汇总()
export const getAccountSummary= params => post('/business/accountSummary', params )

// （六）推广资金池/广告资金池(/business/capitalPool)
export const getCapitalPool= params => post('/business/capitalPool', params )
// 推广资金池/广告资金池列表
export const getCapitalPoolList= params => post('/business/capitalPoolList', params )
// 推广管理菜单
export const getPromotionTreeMenuList= params => post('/business/promotionTreeMenu', params )
// 功能地址映射查询:
export const getAddressMappinguList= params => get('/business/addressMapping', {params} )

