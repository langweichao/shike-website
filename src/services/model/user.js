import { General } from '../request'

const { post, get } = General

// 登录
export const useLogin =  data => post('/dutui/login', data)
//注册
export const useRegister =  data => post('/dutui/register', data)

