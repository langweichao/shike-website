import { Authority } from '../request'

const { post, get } = Authority
//推广管理
// 单元
export const getPromotionUnitList = params => get('/business/promotionUnit', { params })
// 计划
export const getPromotionPlanList = params => get('/business/promotionPlan', { params })
// 项目
export const getOptimizationProjectList = params => get('/business/optimizationProject', { params })

// 优化中心
export const getOptimizationCenterList = params => post('/business/optimizationCenter', params )

// 定向-关键词
export const getPromotionKeywordList = params => get('/business/promotionKeyword', { params })
// 定向-搜索词
export const getPromotionSearchTermList = params => get('/business/promotionSearchTerm', { params })
// 定向-否定关键词
export const getPromotionNegativeKeywordsList = params => get('/business/promotionNegativeKeywords', { params })
// 定向-人群
export const getPromotionCrowdList = params => get('/business/promotionCrowd', { params })
//计划设置
export const getPromotionScheduleSettingsList = params => get('/business/promotionScheduleSettings', { params })


// 创意组-文字类
export const getPromotionCharactersClassList = params => get('/business/promotionCharactersClass', { params })
// 创意组-营销类
export const getPromotionMarketingClassList = params => get('/business/promotionMarketingClass', { params })
//创意组-商品类
export const getPromotionGoodsClassList = params => get('/business/promotionGoodsClass', { params })
// 创意组-应用类
export const getPromotionApplicationClassList = params => get('/business/promotionApplicationClass', { params })
// 创意组-本地类
export const getPromotionLocalClassList = params => get('/business/promotionLocalClass', { params })
// 创意管理
export const getPromotionOriginalityList = params => get('/business/promotionOriginality', { params })
// 创意图片管理
export const getPromotionOriginalityPicList = params => get('/business/promotionOriginalityPic', { params })
// 创意组件视频类
export const getPromotionVideoClassList = params => get('/business/promotionVideoClass', { params })
// 动态创意屏蔽
export const getPromotionOriginalityShieldList = params => get('/business/promotionOriginalityShield', { params })