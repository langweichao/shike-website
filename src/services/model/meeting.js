import { Authority } from '../request'

const { post, get } = Authority
// 会议新增修改
export const meetingAdd = params => post('/meeting/meeting/saveOrUpdate', params)

// 会议列表
export const meetingQueryList = params => get('/open/meeting/queryList', { params })

// 会议列表
export const meetingGetById = params => get('/open/meeting/getById', { params })

// 会议报名
export const applyAdd = params => post('/meeting/user-sign/apply', params)

// 为其他人报名验证码
export const applyOtherMessage = params => post('/meeting/user-sign/applyOtherMessage', params)
//为其他人报名
export const applyOtherAdd = params => post('/meeting/user-sign/applyOther', params)
