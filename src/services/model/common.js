// 一些通用接口 请写在这个文件。  例如查询枚举接口

import { Authority } from '../request'
import constUrl from '../env'

const { get, post } = Authority

// 通过枚举类型获取字典数据
export const getDictData = params => get('/common/dict/v1/getDict', { params })
// 通过枚举类型获取多个字典数据
export const getMoreDictData = params => get('/common/dict/v1/getDictList', { params })

// 获取省列表
export const provinceList = () => get('/common/provinceCityArea/v1/provinceList')

// 获取市列表
export const cityList = params => get('/common/provinceCityArea/v1/cityList', { params })

// 获取区列表
export const areaList = params => get('/common/provinceCityArea/v1/areaList', { params })

// 发送验证码
export const sendCode = params => post('/common/message/v1/sendCode', params)

//通过账号发送验证码
export const sendCodeByLogin = params => post('/common/message/v1/sendCodeByLogin', params)

//忘记密码
export const forgetPassword = params => post('/user/userLogin/v1/forgetPassword', params)


// 上传文件
export const uploadUrl = constUrl + '/common/file/v1/upload'


// 用户列表
export const userQueryList = params => get('/open/user/queryList', { params })
// 专家列表
export const professOpenQueryList = params => get('/profess/open/queryList', { params })
