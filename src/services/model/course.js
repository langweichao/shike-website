// 一些通用接口 请写在这个文件。  例如查询枚举接口

import { Authority } from '../request'
import constUrl from '../env'

const { get, post } = Authority

// 课程列表
export const martQueryList = params => get('/mart/open/queryList', { params })
// 课程详情
export const martGetById = params => get('/mart/open/getById', { params })

// 购物车或者已购买列表
export const martQueryCollectList = params => get('/mart/queryCollectList', { params })


// 商品从购物车购买
export const martBuyByCollect = params => post('/mart/buyByCollect', params)

//商品直接购买
export const martBuyByMart = params => post('/mart/buyByMart', params)

//商品加入购物车
export const martCollect = params => post('/mart/collect', params)

//商品下架
export const martDelete = params => post('/mart/delete', params)

//购物车删除
export const martCollectDelete = params => post('/mart/collectDelete', params)
