import { Authority } from '../request'

const { post, get } = Authority
// 账户整体统计
export const getAccountAllSum = params => get('/business/accountAllSum', { params })


// 账户统计top
export const getactTopSum = params => get('/business/actTopSum', { params })
// 账户整体统计
export const getAccountAllSumList  = params => get('/business/accountAllSum', { params })
export const getActRepostListSum  = params => get('/business/actRepostListSum', { params })


//搜索平台统计列表
export const getActSteamSumList = params => get('/business/actSteamSum', { params })
// 轻舸平台统计列表/
export const getLightVesselSumList = params => get('/business/lightVesselSum', { params })
// TOP5计划与趋势
export const getQueryPlanTop5 = params => post('/business/queryPlanTop5',  params)
// 计划报告列表
export const getPlanReportList = params => get('/business/planReport', { params })
// TOP5单元与趋势
export const unitReportList = params => get('/business/unitReport', { params })
export const queryUnitTop5List = params => post('/business/queryUnitTop5', params )
// 搜索词报告
export const searchTermReportList = params => get('/business/searchTermReport', { params })
export const keywordReportList = params => get('/business/keywordReport', { params })

export const keywordShowTop50List = params => post('/business/keywordShowTop50', params)

// 商品报告
export const goodsReportList = params => get('/business/goodsReport', { params })

// 人群报告
export const crowdReportList = params => get('/business/crowdReport', { params })

// 创意报告
export const originalityReportList = params => get('/business/originalityReport', { params })
// 创意组件报告
export const componentReportList = params => get('/business/componentReport', { params })
// 高级样式报告
export const queryAdvancedStyleReportList = params => get('/business/queryAdvancedStyleReport', { params })
// 视频报告
export const videoReportList = params => get('/business/videoReport', { params })
// 视频报告
export const picReportList = params => get('/business/picReport', { params })
// 小程序报告
export const appletReportList = params => get('/business/appletReport', { params })
// 落地页报告
export const landingReportList = params => get('/business/landingReport', { params })
// 实时报告统计
export const realtimeReportList = params => get('/business/realtimeReport', { params })
// 访客明细
// 数据简报统计
export const dataBriefingSumList = params => get('/business/dataBriefingSum', { params })
// 数据简报搜索词统计
export const dataBriefingSearchSumList = params => get('/business/dataBriefingSearchSum', { params })
// 一键起量报告
export const startingQuantityReportList = params => get('/business/startingQuantityReport', { params })
// ocpc报告（
export const ocpcReportList = params => get('/business/ocpcReport', { params })
// 无效点击报告（
export const invalidClickReportList = params => get('/business/invalidClickReport', { params })
// 无效点击统计（）

export const invalidClickSumList = params => get('/business/invalidClickSum', { params })
//效点击报告top

export const invalidClickTopList = params => get('/business/invalidClickTop', { params })

// 数据简报关键统计
export const dataBriefingKeySumList = params => get('/business/dataBriefingKeySum', { params })

// 访客明细
export const visitorReportList = params => get('/business/visitorReport', { params })
// 数据万花筒
export const dataKaleReportList = params => get('/business/dataKaleReport', { params })
// 地店铺报告
export const localStoreReportList = params => get('/business/localStoreReport', { params })

// 首页广告
export const homeList = params => post('/business/dutuiHome', params )
// TOP5计划与趋势

// 产品动态
export const dynamicsList = params => post('/business/productDynamics',  params )


// 账户概况
export const accountData = params => post('/business/accountInfo', params )