import { Authority ,getBolb} from '../request'

// 个人新增修改

const { get,post} = Authority

// 专家类型列表
export const professTagQueryList = params => get('/open/profess/tag/queryList', { params })

//专家确认(短信确认版)
export const professOpenAdmit = params => post('/profess/open/admit', params)
// 查询专家详情
export const professGetById = params => get('/profess/open/getById', { params })
