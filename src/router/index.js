import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    // {
    //     // 404页面
    //     path: '*',
    //     redirect: '/login',
    //     meta: {
    //         needLogin: true //需要加校检判断的路由
    //     },
    // },
    {
        path: '/',
        redirect: '/login',
        meta: {
            needLogin: true //需要加校检判断的路由
        },

    },
    {
        path: '/login',//path设为空，就是默认展示+的看板
        name: '登录',
        component: () => import('@/views/login/index'),
        meta: { title: '移动crm系统软件_销售管理软件' } // 添加title属性

    },



]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})
router.beforeEach((to, from, next) => {
    // 动态设置页面标题
    document.title = to.meta.title || '默认标题';
    next();
});
export default router
