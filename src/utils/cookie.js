// cookie.js

function setCookie (name, value, timer = 1) { // 默认将被保存 1 天 
  var Days = timer 
  var exp = new Date()
  exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000)
  document.cookie = name + '=' + escape(value) + ';expires=' + exp.toGMTString()
}
 
// name 需要获取cookie的key
 
function getCookie (name) {
  var arr = document.cookie.match(new RegExp('(^| )' + name + '=([^;]*)(;|$)'))
  return arr !== null ? unescape(arr[2]) : null
}
 
// 需要删除cookie的key
function clearCookie (name) {
  var exp = new Date()
  exp.setTime(exp.getTime() - 1)
  var cval = getCookie(name)
  if (cval != null) document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString()
}
 
export default {
  set: setCookie,
  get: getCookie,
  delete: clearCookie
}
