// 登录token凭证
export const USER_TOKEN = 'USER_TOKEN'
// 组织架构树形结构


// 账号类型 1-系统管理员 2-系统管理员子用户 3-租户管理员 4-租户管理员子用户


// 邮箱正则校验
// eslint-disable-next-line no-useless-escape
export const EXG_EMALIL = /^[A-Za-z0-9]+([_\.][A-Za-z0-9]+)*@([A-Za-z0-9\-]+\.)+[A-Za-z]{2,6}$/

// 数字和字母组合
export const EXG_PASSWORD = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,}$/

// 数字、字母、特殊符号 至少2汇总
export const EXG_PASSWORD_SPECAL = /(?!^(\d+|[a-zA-Z]+|[~!@#$%^&*=.?]+)$)^[\w~!@#$%^&*=.?]{8,}$/

// 汉字校验
export const EXG_CHINESE = /^[\u2E80-\u9FFF]+$/

// 匹配以1开头的11位数字手机号校验
export const EXG_PHONE = /^1\d{10}$/

// 只能有数字或字母组成
export const EXG_D_OR_L = /^\w+$/

// 邮编校验
export const EXG_POSTCODE = /^[0-9]{6}$/
// 数字、字母、特殊符号 至少2汇总
export const EXG_PASSWORD_SPECIAL = /(?!^(\d+|[a-zA-Z]+|[~!@#$%^&*=.?+\-—_/]+)$)^[\w~!@#$%^&*=.?+\-—_/]{8,}$/
