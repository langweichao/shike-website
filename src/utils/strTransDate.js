// 字符串转时间戳
export default function strTransDate (mytime) {
  // mytime是待转换时间字符串，格式：'2020-11-26 9:11:23'
  let dateTmp = mytime.replace(/-/g, '/') // 为了兼容IOS，需先将字符串转换为'2020/11/26 9:11:23'
  let timestamp = Date.parse(dateTmp) // 返回'2020-11-26 9:11:23'的时间戳
  return timestamp
}