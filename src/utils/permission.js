/**
 * 格式化时间，按照给定格式输出时间  例如：format(new Date()):  2020-04-13 14:41:51
 * @param {*} date 输入的date对象
 * @param {*} fmt 需要转换的时间格式
 */
export default function permission (userInfo,permissionStr) { // author: meizz
  let permissionList = userInfo.permissionList || []
  return permissionList.indexOf(permissionStr) !== -1
}
