const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true, // axios.default是否启用对依赖项的转译

  // 指定构建时的输出目录
  outputDir: 'dist',
  // 指定放置静态资源的目录
  assetsDir: 'static',
  // 在开发环境下启用 eslint-loader 保存时的 lint 检查
  lintOnSave: process.env.NODE_ENV === 'development',

  // 开发服务器配置
  devServer: {
    proxy: {
      '/api': {
        // target: 'https://2527903x4v.goho.co', // 后台接口的目标地址
        target: 'http://lzx.v7.idcfengye.com', // 后台接口的目标地址

        // target: 'http://lzx.v7.idcfengye.com/prod-api', // 后台接口的目标地址
        changeOrigin: true, // 是否改变请求的来源，用于跨域设置
        pathRewrite: {
          '^/api': '/prod-api', // 重写请求路径
        },
      },
    },
  },

  // 设置构建后的公共路径为根目录
  publicPath: '/',

  // 是否生成生产环境的源映射（根据实际需求设置）
  productionSourceMap:false,
});
